#!/usr/bin/env python3

import os
import sys
import re
import asyncio
from collections import ChainMap
from argparse import ArgumentParser, Namespace
from getpass import getpass
from nio import Api, AsyncClient, SyncResponse, RoomMessageText
from markdown2 import Markdown

DEFAULTS = {
    'hs': os.getenv('MATRIX_SERVER'),
    'user': os.getenv('MATRIX_USER'),
    'room': os.getenv('MATRIXCLI_ROOM'),
    'name': None,
    'otoken': False,
    'device': 'matrixcli',
    'pw': os.getenv('MATRIX_PASSWORD'),
    'itoken': os.getenv('MATRIX_ACCESS_TOKEN'),
}

room_regex = re.compile('(#|!).+?:.+', re.IGNORECASE)


async def stream_as_generator(loop, stream):
    reader = asyncio.StreamReader(loop=loop)
    reader_protocol = asyncio.StreamReaderProtocol(reader)
    await loop.connect_read_pipe(lambda: reader_protocol, stream)

    while True:
        line = await reader.readline()
        if not line:  # EOF.
            break
        yield line.decode('utf-8')

async def connect(hs, user, name, otoken, device, pw, itoken):
    client = AsyncClient(f'https://{hs}', user=f'@{user}:{hs}',
                         device_id=device)

    if itoken:
        client.access_token = itoken
    else:
        session = await client.login(getpass() if pw == None else pw)
        if name:
            displayname = await client.set_displayname(name)
        if otoken:
            print(client.access_token)
        if name or otoken:
            sys.exit(0)

    return client

async def join(client, hs, room):
    if room:
        if room_regex.match(room):
            room_id = room
        else:
            room_id = ''.join(['#', room, ':', hs])

    return await client.join(room_id)

async def read_file(loop, fin, queue):
    async for line in stream_as_generator(loop, fin):
        await queue.put(line)
        print(f"Reader adds <{line.strip()}> to queue_in.")

async def send_message(client, joined, queue):
    while True:
        line = await queue.get()
        print(f"Sender got <{line.strip()}> from queue_in.")
        content = {
            'msgtype': 'm.text',
            'format': 'org.matrix.custom.html',
            'body': line,
            'formatted_body': Markdown().convert(line)
        }
        await client.room_send(joined.room_id, 'm.room.message', content)
        queue.task_done()

async def main(hs, user, room, name, otoken, device, pw, itoken):
    client = await connect(hs, user, name, otoken, device, pw, itoken)
    joined = await join(client, hs, room)

    loop = asyncio.get_event_loop()
    queue_in = asyncio.Queue()

    if sys.version_info >= (3, 7):
        reader = asyncio.create_task(read_file(loop, sys.stdin, queue_in))
        sender = asyncio.create_task(send_message(client, joined, queue_in))
    else:
        reader = asyncio.ensure_future(read_file(loop, sys.stdin, queue_in))
        sender = asyncio.ensure_future(send_message(client, joined, queue_in))

    await asyncio.gather(reader)
    await queue_in.join()  # Implicitly awaits consumers, too
    sender.cancel()

    await client.close()

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-s', '--homeserver', dest='hs',
                        help='homeserver name without url part')
    parser.add_argument('-u', '--user', dest='user',
                        help='localpart of the username without @')
    parser.add_argument('-r', '--room', dest='room',
                        help='room id, alias or alias localpart')
    parser.add_argument('-n', '--name', dest='name',
                        help='set displayname and exit')
    parser.add_argument('-a', '--access-token', dest='otoken',
                        action='store_const', const=True,
                        help='print access_token and exit')
    parser.add_argument('-d', '--device', dest='device', help='device id')
    argument_values = vars(parser.parse_args())
    arguments = {k: v for k, v in argument_values.items() if v}
    ns = Namespace(**dict(ChainMap(arguments, DEFAULTS)))
    try:
        if sys.version_info >= (3, 7):
            asyncio.run(main(**ns.__dict__))
        else:
            asyncio.get_event_loop().run_until_complete(main(**ns.__dict__))
    except KeyboardInterrupt:
        pass
    finally:
        # How to `await client.close()` here?
        asyncio.get_event_loop().close()
