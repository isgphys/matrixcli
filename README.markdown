# matrixcli

A simple [matrix](https://matrix.org) cli client using [matrix-nio](https://github.com/poljar/matrix-nio)

## Installation

### Python 3.7

```bash
git clone git@gitlab.phys.ethz.ch:matrix/matrixcli.git
cd matrixcli
python3.7 -m venv .venv3.7
source .venv3.7/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
./matrixcli.py -h
```

### Python 3.6

```bash
git clone git@gitlab.phys.ethz.ch:matrix/matrixcli.git
cd matrixcli
python3.6 -m venv .venv3.6
source .venv3.6/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
./matrixcli.py -h
```

## Environment Variables

Variable | Description
---------|------------
`MATRIX_SERVER` | homeserver name without url part
`MATRIX_USER` | localpart of the username without @
`MATRIX_PASSWORD` | password
`MATRIX_ACCESS_TOKEN` | access_token for non-interactive mode
`MATRIXCLI_ROOM` | room id, alias or alias localpart

## Usage

```
usage: matrixcli.py [-h] [-s HS] [-d DEVICE] [-u USER] [-r ROOM] [-n NAME]
                    [-a] [--debug]

optional arguments:
  -h, --help            show this help message and exit
  -s HS, --homeserver HS
                        homeserver name without url part
  -d DEVICE, --device DEVICE
                        device id
  -u USER, --user USER  localpart of the username without @
  -r ROOM, --room ROOM  room id, alias or alias localpart
  -n NAME, --name NAME  set displayname and exit
  -a, --access-token    print access_token and exit
  --debug               enable debug mode
```

Example:

```bash
echo 'Hello from `matrixcli`.' | ./matrixcli.py -s 'matrix.phys.ethz.ch' -u test-bot -r bot-room
Password: P4$$w0rd
```

Non-interactive example:

```bash
./matrixcli.py -s 'matrix.phys.ethz.ch' -u test-bot -a -n fancybotname
Password: P4$$w0rd
MDAyMWxvY2F0aW9uIG1hdHJpeC5waHlzLmV0aHouY2gKMDAx...
export MATRIX_SERVER=matrix.phys.ethz.ch
export MATRIX_USER=test-bot
export MATRIXCLI_ROOM=bot-room
 export MATRIX_ACCESS_TOKEN=MDAyMWxvY2F0aW9uIG1hdHJpeC5waHlzLmV0aHouY2gKMDAx...
echo 'Hello from non-interactive `matrixcli`.' | ./matrixcli.py
tail -n 0 -f test.log | ./matrixcli.py
```

## Log tailer bot example

- Clone and install (see **Installation**) to `/opt/matrixcli`
- Create an access token
- Create the bot script `/opt/matrixcli/test-bot.sh`:

```bash
#!/bin/bash

export MATRIX_SERVER=matrix.phys.ethz.ch
export MATRIX_USER=test-bot
export MATRIXCLI_ROOM=bot-room
export MATRIX_ACCESS_TOKEN=MDAyMWxvY2F0aW9uIG1hdHJpeC5waHlzLmV0aHouY2gKMDAx...

tail -n 0 -f /var/log/syslog | grep --line-buffered -iE "Processed request.+?user_directory/search" | /opt/matrixcli/.venv/bin/python3 /opt/matrixcli/matrixcli.py
```

- Create a systemd service `/etc/systemd/system/matrix-matrixcli-test.service`:

```bash
[Unit]
Description="Matrix matrixcli test"

[Service]
WorkingDirectory=/opt/matrixcli
ExecStart=/opt/matrixcli/test-bot.sh
SyslogIdentifier=matrix-matrixcli-test

Restart=always

[Install]
WantedBy=default.target
```

- Enable and start the service:

```bash
systemctl daemon-reload
systemctl enable matrix-matrixcli-test.service
systemctl start matrix-matrixcli-test.service
```
